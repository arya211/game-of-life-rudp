# This code is
#   based on https://nostarch.com/download/samples/PythonPlayground_sampleCh3.pdf
#   prepared for Computer Networking Class: Ferdowsi University of Mashhad
import hashlib
import pickle
import random
import string
import time
import uuid

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import socket

recv_host = '127.0.0.1'
recv_port = 3030
recv = (recv_host, recv_port)
# create the client socket we want to use
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.settimeout(0.0015)
# initialize variables
length = total_length = 500
sequence_number = 1
matplotlib.use("TkAgg")  # For pycharm IDE only
N = 30  # Grid size is N*N
live = 255
dead = 0
state = [live, dead]
M = 0
# Create random population (more dead than live):
grid = np.random.choice(state, N * N, p=[0.3, 0.7]).reshape(N, N)


# To learn more about not uniform random visit:
# https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.choice.html
def sendToServer(msg):
    global sequence_number, length, total_length
    data = msg[0:length]  # this and next line basically do what a msg.read(length) would do
    msg = msg[length:]  # get after 500 strings of msg..
    all_data_read = False  # checks if all data is read or isn't
    timeout = 0.015  # initialize the timeout as 1.5 seconds

    while not all_data_read:
        # we must read the data before the loop,or the first transmission wouldn't
        # know whether data_fin_state condition
        snd_pkt = [sequence_number, data]  # list of the packet segments
        # we use sha1 over md5 because of more security
        sha1 = hashlib.sha1()  # here we define sha1.hashlib algorithm for the CHECKSUM
        sha1.update(pickle.dumps(snd_pkt))
        snd_pkt.append(sha1.digest())  # the checksum

        pkt = pickle.dumps(snd_pkt)  # encoding for sending
        # send data
        client_socket.sendto(pkt, (recv_host, recv_port))
        last_ackrecv = time.time()  # timer start to calculate

        print(f"Sent data(seq_num): {sequence_number}")

        if (not data):  # if data is empty,data=False
            all_data_read = not all_data_read  # set the statement flag be True

        # RECEIPT OF AN ACK
        try:
            packet, serverAddress = client_socket.recvfrom(4096)
            recv_pkt = []
            recv_pkt = pickle.loads(packet)  # decoding the recieved packet
            # check whether we received the right packet
            chksum = recv_pkt[-1]
            del recv_pkt[-1]
            sha1_rec = hashlib.sha1()
            sha1_rec.update(pickle.dumps(recv_pkt))
            if chksum == sha1_rec.digest():
                print(f"Received ack: {recv_pkt[0]}")
                # only after receiving the data,then we can transmit the next one
                sequence_number += 1
                total_length += length
                data = msg[0:length]  # this and next line basically do what a msg.read(length) would do
                msg = msg[length:]
                # we use an adaptive timeout
                time_recv = time.time()  # until we get the packet,we ensure it's a round
                newRTT = time_recv - last_ackrecv  # calculate the RTT
                timeout = 0.9 * timeout + 0.1 * newRTT
                # print(f'timeout: {timeout} \n')

            else:  # if the packet number is not corresponding,then we didn't receive the right packet
                print("error detected")

        # TIMEOUT
        except:  # if we can't get the revc,check if it has reach the timeout
            if (time.time() - last_ackrecv > timeout):
                client_socket.sendto(pkt, recv)
                print("Timeout happened!")
                print(f"resent data: {sequence_number}")


def update(data):
    global grid
    temp = grid.copy()
    will_die = []  # we want to choose between the cells that will die next turn
    for i in range(N):
        for j in range(N):
            # Compute 8-neighbor sum
            total = (grid[i, (j - 1) % N] + grid[i, (j + 1) % N] +
                     grid[(i - 1) % N, j] + grid[(i + 1) % N, j] +
                     grid[(i - 1) % N, (j - 1) % N] + grid[(i - 1) % N, (j + 1) % N] +
                     grid[(i + 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j + 1) % N]) / 255
            # Apply Conway's Rules:
            # 1- Any live cell with fewer than two live neighbours dies, as if by underpopulation.
            # 2- Any live cell with two or three live neighbours lives on to the next generation.
            # 3- Any live cell with more than three live neighbours dies, as if by overpopulation.
            # 4- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            if grid[i, j] == live:
                if (total < 2) or (total > 3):
                    will_die.append((i, j))
                    temp[i, j] = dead
            else:
                if total == 3:
                    temp[i, j] = live
    mat.set_data(temp)
    position = random.choice(will_die)
    id = uuid.uuid1()  # create an id for the dying cell
    msg = ''.join(random.choice(string.ascii_lowercase) for x in range(random.randrange(500, 1500)))
    msg += 'UUID:' + repr(id.int) + 'POSITION:' + f'{position[0], position[1]}'
    msg = msg.encode('utf-8')  # now we got the entire message we want to send! lets encode it...
    sendToServer(msg)
    grid = temp
    return mat


# Animation
fig, ax = plt.subplots()
mat = ax.matshow(grid)
ani = animation.FuncAnimation(fig, update, interval=500)
plt.show()

print("connection closed")
client_socket.close()
