#!/usr/bin/env python
import socket
import pickle
import hashlib
import time

# takes the port number as command line arguments and create server socket
serverIP = "127.0.0.1"
serverPort = 3030

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server.settimeout(2)
server.bind((serverIP, serverPort))
print("Waiting to serve...")

# initializes packet variables, both ACK and seqnum start off as 1
expected_seq_num = 1
# RECEIVES DATA
all_data_read = False  # checksum which defines whether finish reading
msg = ''
last_pkt_recv = time.time()

while True:
    try:
        packet, clientAddress = server.recvfrom(4096)
        recv_pkt = pickle.loads(packet)  # decode the recieved packet

        # check whether the received checksum value are the same as its sequence number
        chksum = recv_pkt[-1]  # the last part of the received packet which is the checksum
        del recv_pkt[-1]

        sha1 = hashlib.sha1()
        sha1.update(pickle.dumps(recv_pkt))
        # check whether the received packet's sequence number is in order
        if chksum == sha1.digest():  # No error happens because the checksum is the same!
            # create ACK by: (seqnum,checksum of that seqnum)
            ack_pkt = [recv_pkt[0]]
            sha1_send = hashlib.sha1()
            sha1_send.update(pickle.dumps(ack_pkt))
            ack_pkt.append(sha1_send.digest())
            ack_pkt = pickle.dumps(ack_pkt)
            # time.sleep(0.2)
            # ack.pkt = pickle.dumps()
            server.sendto(ack_pkt, clientAddress)  # send ack_pkt to client
            # print(f"New Ack: {recv_pkt[0]}")
            # then check if the packet is what we want
            if recv_pkt[0] == expected_seq_num:  # In order
                # print(f"Accept {recv_pkt[0]}")
                if recv_pkt[1]:  # the second part is the data,but only if it's not empty
                    msg = msg + recv_pkt[1].decode('utf-8')
                else:  # this means all the data of the message has been read
                    all_data_read = True
                    msg = msg[-70:]
                    print(
                        f"UUID: {((msg.split('UUID:'))[1].split('POS')[0])} ~|~ Position: {msg.rsplit('POSITION:', 1)[1]}")

                expected_seq_num += 1  # update the expected number


            else:  # right packet wasn't recieved
                print('Not in place!\n this packet has been Ignored')

        else:  # no corresponding
            print('checksum isn\'t the same! \n this packet has been Ignored')

    except:
        if all_data_read:  # If reach the end of the data
            if time.time() - last_pkt_recv > 0.15:  # if we waited more than 1.5 seconds, then break
                break

endtime = time.time()

print("File Transfer Successful!")
